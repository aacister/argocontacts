var gulp = require('gulp');
var browserify = require('browserify');
var babelify      = require('babelify');
var ngAnnotate    = require('browserify-ngannotate');
var watchify = require('watchify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var concat = require('gulp-concat');
var gutil = require('gulp-util');
var browserSync = require('browser-sync');
var notify        = require('gulp-notify');
var uglify = require('gulp-uglify');
var merge   = require('merge-stream');


var interceptErrors = function(error) {
  var args = Array.prototype.slice.call(arguments);

  // Send error to notification center with gulp-notify
  notify.onError({
    title: 'Compile Error',
    message: '<%= error.message %>'
  }).apply(this, args);

  // Keep gulp from hanging on this task
  this.emit('end');
};


gulp.task('browserify', function() {
  return browserify('./src/index.js')
  .transform(babelify, {presets: ['es2015','react','stage-2']})
  .transform('babelify')
  .transform(ngAnnotate)
  .bundle()
  .on('error', interceptErrors)
  //Pass desired output filename to vinyl-source-stream
  .pipe(source('bundle.js'))
  // Start piping stream to tasks!
  .pipe(gulp.dest('./build/'));

});

gulp.task('html', function() {
  return gulp.src("./src/index.html")
      .on('error', interceptErrors)
      .pipe(gulp.dest('./build/'));
});

gulp.task('build', ['html', 'browserify'], function() {
  var html = gulp.src("./build/index.html")
                 .pipe(gulp.dest('./dist/'));

  var js = gulp.src("./build/bundle.js")
               .pipe(uglify())
               .pipe(gulp.dest('./dist/'));

  return merge(html,js);
});

gulp.task('default', ['html', 'browserify', 'build'], function() {

  browserSync.init(['./build/**/**.**'], {
    server: "./build",
    port: 4000,
    notify: false,
    ui: {
      port: 4001
    }
  });
  });
