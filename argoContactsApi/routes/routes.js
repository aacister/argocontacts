var express = require('express');
var router = express.Router();
var cassandra = require('cassandra-driver');

var client = new cassandra.Client({contactPoints: ['127.0.0.1']});
client.connect(function(err, result){
  console.log('routes: cassandra connected.');
});

var getAllContacts = 'SELECT * from argo.contacts';
var getContactById = 'SELECT * from argo.contacts where id = ?';
var deleteContact = 'DELETE FROM argo.contacts where id = ?';

/* GET home page. */
router.get('/', function(req, res, next) {
  console.log('found root route.');
  res.render('index', { title: 'Express for ArgoContacts' });
});

//Get all contacts
router.get('/api/contacts/', (req, res, next) => {
  client.execute(getAllContacts, [], function(err, result){
    if(err){
      res.status(400).send({msg: err});
    }else {
      res.json(result.rows);
    }
  });
});

//Get contact by id
router.get('/api/contacts/:id', function(req, res, next) {
  client.execute(getContactById, [req.params.id], (err, result)=>{
    if(err){
      res.status(400).send({msg: err});
    }
    else
    {
        res.json(result.rows);
    }
  })
});

//Add contact
router.post('/api/contacts/', (req, res) =>{
  var id = cassandra.types.uuid();
  var upsertContact = 'INSERT INTO argo.contacts(id, email, first_name, last_name) VALUES (?,?,?,?)';

  client.execute(upsertContact, [id, req.body.contact.email, req.body.contact.first_name, req.body.contact.last_name],
   (err, result)=>{
      if(err){
        res.status(400).send({msg: err});
      }
      else
      {
        client.execute(getContactById, [id], (e, r)=>{
          if(err){
            res.status(400).send({msg: e});
          }
          else
          {
              console.log('Results:' + JSON.stringify(r));
              res.json(r.rows[0]);
          }
        })

      }
    })
});

//Edit contact
router.put('/api/contacts/:id', (req, res)=>{

  var upsertContact = 'INSERT INTO argo.contacts(id, email, first_name, last_name) VALUES (?,?,?,?)';
  client.execute(upsertContact, [req.params.id, req.body.contact.email, req.body.contact.first_name, req.body.contact.last_name],
   (err, result)=>{
      if(err){
        res.status(400).send({msg: err});
      }
      else {
        client.execute(getContactById, [req.params.id], (e, r)=>{
          if(err){
            res.status(400).send({msg: e});
          }
          else
          {
              console.log('Results:' + JSON.stringify(r));
              res.json(r.rows[0]);
          }
        })
      }
    })

});


router.delete('/api/contacts/:id', (req, res)=>{
  client.execute(deleteContact, [req.params.id], function(err, result){
    if(err)
      res.status(400).send({msg: err});
    else {
      res.json(result.rows);
    }
  })
});

module.exports = router;
