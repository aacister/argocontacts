# argoContacts
A React Redux application that consumes data from the Node/Express 4 API. Users can create, edit, delete, filter and view contacts.

Uses:

React
Redux
React Router
Redux Thunk middleware
NodeJS
ExpressJS
Gulp
Browserify
Babel

To Run Locally:

Cassandra setup in cql shell:

1) CREATE KEYSPACE argo WITH REPLICATION - {'class' : 'SimpleStrategy', 'replication_factor' : 3};

2) CREATE TABLE contacts (
    id uuid,
    email text,
    first_name text,
    last_name text,
    PRIMARY KEY (id)
);

3) Clone repository

In argoContactsAPI directory:

4) npm install

5) npm start

In argoContactsClient directory:

6) npm install 

7) gulp

8) Go to http://localhost:4000/   (Gulp should start it automatically)